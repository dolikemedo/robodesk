# Robodesk

Robodesk is a virtual desktop environment in docker containers.\
All containers are specific ROS distro enabled. You have to select a branch for your purposes\
You can connect to the virtual desktop with a remote desktop client (through VNC) or with your browser (through Xpra)\
You can also use GUI applications with the "run-gpu.sh" method and run apps locally without connecting to the virtual desktop.\
For further information pls contact the devs. 
- Szabó Péter (szabopeter@sztaki.hu), 
- Soós Péter (sopesa@sztaki.hu, sopesa), 
- Marosi Attila (atisu@sztaki.hu, atisu)

Everything is in developement so if you have some ideas/needs pls contact us.


## Getting started

A few source to check:

[http://wiki.ros.org/rviz](http://wiki.ros.org/rviz)

[https://gazebosim.org/](https://gazebosim.org/)

[https://clearpathrobotics.com/](https://clearpathrobotics.com/)


### Prerequisite:
- Basic Docker knowledge (check TKP-V Wiki for more)
- Basic ROS1 and ROS2 knowledge (check TKP-V Wiki for more)
- Your coding skills (Python, C, C++)
- Some networking (IP network basics)

### Branches:
- Base: Containers with main ROS distros. This is a base point for your experiments and additional developements for specific goals.
- dev : dev branches are developer specific and have the name of the dev. Discuss with the developer.
- husky-sim: Gazebo Simulation environment with Husky robot.
- husky-sim-vnc: -,,- VNC implemented.
- mavros: robodesk with mavros capabilities (in developement)

## Installation
- Install Docker [exmpl.: Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- Prepare your network if host is remote, locally it's fine with network=host.
- Select a branch for your purposes and check additional readmes. clone the branch

	### for Nvidia Containers (run-gpu.sh), NVIDIA Container Toolkit:
	- Install latest proprietery nvidia drivers from ubuntu "Software & Update" 
	- Follow the [official guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker), steps here might not work, this is not the offical maintained guide.
	- Setup the package repository and the GPG key:
		```
		distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
		```
	- Install the nvidia-container-toolkit package (and deps):
    	```
		sudo apt-get update
		sudo apt-get install -y nvidia-container-toolkit

		```
	- Configure the Docker daemon to recognize Nvidia container runtime:
    	```
		sudo nvidia-ctk runtime configure --runtime=docker
		```
	- Restart, Test:
		```
		sudo systemctl restart docker
		sudo docker run --rm --runtime=nvidia --gpus all nvidia/cuda:11.6.2-base-ubuntu20.04 nvidia-smi
		```
	(find a suitable image for your OS distro if above is not working)


## Usage

- run compile.sh then run.sh. The scripts will help you. compile.sh help, run.sh help
- Get to know Docker a little. If you have, than you can install packages, apps, code, etc. You can grow the container runtime. 
  - exec into the container, and you can run apt, apt-get, git etc. 'docker exec -it robodesk-container-name bash'
  - this way if you delete the container or compile a new with the same name:tag, be mindful your changes will be lost.
  - If you grow a container this way give it a unique name. You can still stop and start it. If you dont need it anymore you can delete it ofcourse.
- After you have done interacting with the base container you should write your changes in the Dockerfile.
- If you are confident create a branch and push your robodesk version if only for yourself, use dev prefix.

	### Remote Desktop:
	- remotedesktop Xpra: host_ip:10000  (you can change the exposed port, see dockerfile)
	- VNC is also available, just edit the entrypoint script, or launch manually from "user"
  	- VNC issue: If display seems to be "used", then exec into container, "su user" and vncserver -kill :0, -kill :1.. etc. try again.
	- Dont forget to set screensaver to *never* or you might get locked out.

	![xfce power manager](/images/xfcepowermanager0.png)
	![xfce power manager](/images/xfcepowermanager_scaled.png)

	### Usage with GPU: 
	- run-gpu.sh will grant access to the GPU for the container and your native display.
	- It is the currently the fastest solution, but I'll improve on remotedesk with VirtualGL and turboVNC :) 

## Authors and acknowledgment
Budapest, BME TMIT
Budapest, SZTAKI SCL

## License
Open source, free ?? I would have to check with legal guys over this.\
but everything is learned and aquired from the internet, forums, sites, youtube FOR FREE\
So besides the specific project work, everything else should also be free and the knowledge should be distributed freely.\
hastagfreeinternet

